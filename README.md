# Veille techno  


## Actus 

- https://news.humancoders.com/ 
- http://www.creativejuiz.fr/blog/

## Inspiration graphique

- https://www.awwwards.com/


## Gestion de projet 
- https://app.asana.com/
- https://trello.com/

## Graphisme 

- https://avocode.com/

## SEO 

- https://fr.slideshare.net/JCDblog/guide-pour-trouver-les-bons-mots-cls-audit-lexical-seo


## tutos 

- https://code.tutsplus.com/tutorials
- https://www.grafikart.fr/

## Développement 

### Javascript

### VueJs
- https://www.udemy.com/vuejs-2-the-complete-guide/
- https://blog.sqreen.io/authentication-best-practices-vue/
- https://madewithvuejs.com/ui-components
- https://learninglaravel.net/topics/vuejs?page=3
- https://scotch.io/tutorials/build-a-guestbook-with-laravel-and-vuejs
### PHP 

### .net 

### HTML 

### CSS

- http://rebar.io/


## CMS 

### Wordpress

- http://generatewp.com/taxonomy/
- https://www.creativejuiz.fr/blog/theme/wordpress
- https://www.journaldunet.com/solutions/expert/68952/wordpress---10-extensions-de-conformite-rgpd--gdpr.shtml
- http://underscores.me/
## DevOps

### Docker

### Vagrant 

### Ansible
